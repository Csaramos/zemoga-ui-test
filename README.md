# ZemogaUiTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

###### date functionality
** The app calculate how many days left to finish the month, that's the amount shown in part "XX Days" part of header in the home page **

## firebase
This project use firebase to persist data, the app take the credentials from an file that must be located in root folder(src), the file with credentials it's going to be provided via email.

Additional, i want to mention that  basics rules of security in firebase was  implemented to prevent issues in set incorrect data or create new collections.

## installation
Run `npm install` for install all dependencies
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
