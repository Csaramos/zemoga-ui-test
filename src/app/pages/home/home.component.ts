import { Component, OnInit } from '@angular/core';
import { HomeCopies } from 'src/app/models/home-copies.model';
import { VotesInfo } from 'src/app/models/votes-info.model';
import { HomeService } from 'src/app/services/home.service';
import { VotesService } from 'src/app/services/votes-service';
import { UtilService } from 'src/app/shared/utils/util-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  homeCopies: HomeCopies;
  votesInfo: VotesInfo[] = [];
  showCard: boolean;

  constructor(
    private votesInfoService: VotesService,
    private homeInfoService: HomeService,
    private utiService: UtilService
  ) {
    this.showCard = true;
  }

  ngOnInit(): void {
    this.getHomeCopies();
    this.getVotesInfo();
  }

  /**
   * Set home copies
   * @author cramos
   */
  getHomeCopies(): void {
    this.homeInfoService.getHomeCopies().subscribe((resp: HomeCopies) => {
      if (resp) {
        this.homeCopies = {
          ...resp,
          clousingDate: this.getRemainingDays()
        };
      } else {
        this.utiService.catchException(resp);
      }
    }, err => {
      this.utiService.catchException(err);
    });
  }

  /**
   * Calculate remaining days till last day of current month
   * @author cramos
   */
  getRemainingDays(): string {
    const currentDay = new Date();
    const lastDay = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0).getDate();
    return `${lastDay - currentDay.getDate()} days`;
  }

  /**
   * get votes info from service
   * @author cramos
   */
  getVotesInfo(): void {
    this.votesInfoService.getVotesInfo().subscribe(res => {
      this.votesInfo = res.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as VotesInfo;
      });
    }, err => {
      if (err) {
      }
    });
  }
}
