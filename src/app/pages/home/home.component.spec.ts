import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';
import { of } from 'rxjs';
import { HomeService } from 'src/app/services/home.service';
import { VotesService } from 'src/app/services/votes-service';

import { HomeComponent } from './home.component';

const data = [
  {
    payload: {
      doc: {
        id: 'ASDAC2323AS',
        data: () => {
          return {
            name: 'Kanye West',
            votes: {
              likes: 1,
              dislikes: 1
            },
            timeInProfetion: '1 mounth ago',
            profession: 'Entretaiment',
            description: 'Half-giant jinxes peg-leg gillywater broken glasses large black dog Great Hall. Nearly-Headless Nick now string them together, and answer me this',
            urlPicture: '/Kanye.png'
          };
        }
      }
    },
  },
];
const collectionStub = {
  valueChanges: jasmine.createSpy('valueChanges').and.returnValue(data)
};
const angularFiresotreStub = {
  collection: jasmine.createSpy('collection').and.returnValue(collectionStub)
};
describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let angularFirestore: AngularFirestore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: AngularFirestore, useValue: angularFiresotreStub }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
    angularFirestore = TestBed.inject(AngularFirestore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    const spyService = TestBed.inject(VotesService);
    spyOn(spyService, 'getVotesInfo').and.returnValue(of(data));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * unit test getRemainingDays, should return string
   * @author cramos
   */
  it('getRemainingDays should calculate remaining days', () => {
    expect(component.getRemainingDays()).toContain('days');
  });

  /**
   * unit test getHomeCopies, should set home copies
   * @author cramos
   */
  it('getHomeCopies should get and set copies of home', () => {
    const mockedHomeData = {
      dateLabel: 'CLOSING IN',
      tittle: 'Pope Francis?',
      littleTittle: 'Whats your opinion on',
      mainText: 'Hes talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontifill? (thumbs up)',
      question: 'Whats Your Veredict'
    };
    const homeServiceMock = TestBed.inject(HomeService);
    spyOn(homeServiceMock, 'getHomeCopies').and.returnValue(of(mockedHomeData));
    component.getHomeCopies();
    expect(component.homeCopies).toBeDefined();
    expect(component.homeCopies.dateLabel).toBe(mockedHomeData.dateLabel);
  });
});
