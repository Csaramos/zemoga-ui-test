import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HomeService } from './home.service';
import { HomeCopies } from '../models/home-copies.model';

describe('HomeService', () => {
  let service: HomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  /**
   * unit test getHomeCopies, should return home copies
   * @author cramos
   */
  it('getHomeCopies should return copies of home', () => {
    service.getHomeCopies().subscribe((res: HomeCopies) => {
      expect(res.dateLabel).toBeDefined();
      expect(res.dateLabel).toBe('CLOSING IN');
    });
  });

});
