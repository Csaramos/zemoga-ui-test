import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { COLLECTIONS } from 'src/assets/constants/constants';
import { VotesInfo } from '../models/votes-info.model';

@Injectable({
  providedIn: 'root'
})
export class VotesService {
  votesInfo: Observable<any>;

  constructor(
    private firestore: AngularFirestore
  ) { }

  /**
   * Validate if information exist, otherwise get info from server
   * @author cramos
   */
  getVotesInfo(): Observable<any> {
    if (this.votesInfo) {
      return this.votesInfo;
    }
    this.votesInfo = this.firestore.collection(COLLECTIONS.VOTES).snapshotChanges();
    return this.votesInfo;
  }

  /**
   * update the information in the server
   * @param currentVotesInfo updated information
   * @author cramos
   */
  setVotesInfo(currentVotesInfo: VotesInfo): Promise<boolean> {
    const requestEndPoint = `${COLLECTIONS.VOTES}/${currentVotesInfo.id}`;
    delete currentVotesInfo.id;
    const dataTouptade = {
      votes: currentVotesInfo.votes
    };
    return this.firestore.doc(requestEndPoint).update(dataTouptade).then(() => true)
      .catch(() => false);
  }
}
