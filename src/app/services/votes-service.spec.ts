import { TestBed } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';

import { VotesService } from './votes-service';

const data = {
  id: '',
  name: '',
  votes: '',
  timeInProfetion: '',
  profession: '',
  description: '',
  urlPicture: ''
};

const collectionStub = {
  valueChanges: jasmine.createSpy('valueChanges').and.returnValue(data)
};

const angularFiresotreStub = {
  collection: jasmine.createSpy('collection').and.returnValue(collectionStub)
};
describe('VotesService', () => {
  let service: VotesService;
  let angularFirestore: AngularFirestore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AngularFirestore, useValue: angularFiresotreStub }
      ]
    });
    service = TestBed.inject(VotesService);
    angularFirestore = TestBed.inject(AngularFirestore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
