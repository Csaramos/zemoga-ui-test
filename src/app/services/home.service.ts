import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { URLSCOPIES } from 'src/assets/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private homeContent: Observable<object>;

  constructor(private http: HttpClient) { }

  /**
   * service to get copies of home page
   * @author cramos
   */
  getHomeCopies(): Observable<object> {
    if (!this.homeContent) {
      this.homeContent = this.http.get(URLSCOPIES.HOME);
    }
    return this.homeContent;
  }
}
