import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-informative-card',
  templateUrl: './informative-card.component.html',
  styleUrls: ['./informative-card.component.scss']
})
export class InformativeCardComponent {
  @Output() hideInformativeCard = new EventEmitter();


  emitHideCard(): void {
    this.hideInformativeCard.emit();
  }
}
