import { TestBed } from '@angular/core/testing';

import { UtilService } from './util-service';

describe('UtilService', () => {
  let service: UtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   * unit test getPercentage should return 50
   * @author cramos
   */
  it('getPercentage should return positive percentage', () => {
    const mockedVotes = {
      likes: 1,
      dislikes: 1
    };
    expect(service.getPercentage(mockedVotes, true)).toBe('50');
  });

  /**
   * unit test getPercentage should return 20
   * @author cramos
   */
  it('getPercentage should return negative percentage', () => {
    const mockedVotes = {
      likes: 80,
      dislikes: 20
    };
    expect(service.getPercentage(mockedVotes, false)).toBe('20');
  });
});
