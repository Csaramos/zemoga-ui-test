import { Injectable } from '@angular/core';
import { VotesTypes } from 'src/app/models/votes-info.model';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  /**
   * calculate percentage based on current votes
   * @param votes votes to calculate
   * @param like like or dislike
   * @author cramos
   */
  getPercentage(votes: VotesTypes, like: boolean): string {
    const likes = votes.likes;
    const dislikes = votes.dislikes;
    const total = likes + dislikes;
    const result = like ? (likes * 100) / total : (dislikes * 100) / total;
    return result.toFixed(0);
  }

  /**
   * generic method to handle errors
   * @param err error description
   * @author cramos
   */
  catchException(err: any): void {
    console.log('navigate to error', err);
  }
}

