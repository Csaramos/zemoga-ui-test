import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';
import { UtilService } from '../utils/util-service';
import { VoteCardComponent } from './vote-card.component';

const data = {};
const collectionStub = {
  valueChanges: jasmine.createSpy('valueChanges').and.returnValue(data)
};
const angularFiresotreStub = {
  collection: jasmine.createSpy('collection').and.returnValue(collectionStub)
};
describe('VoteCardComponent', () => {
  let component: VoteCardComponent;
  let fixture: ComponentFixture<VoteCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VoteCardComponent],
      providers: [
        UtilService,
        { provide: AngularFirestore, useValue: angularFiresotreStub }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * unit test ngOnInit should thumbUpIcon be true
   * @author cramos
   */
  it('ngOnInit should set percentage copies', () => {
    const mockedVotesInfo = {
      id: '123',
      name: 'name',
      votes: {
        likes: 90,
        dislikes: 10
      },
      timeInProfetion: '10 months',
      profession: 'research',
      description: 'ops',
      urlPicture: '/some'
    };
    component.voteInfo = mockedVotesInfo;
    component.ngOnInit();
    expect(component.thumbUpIcon).toBeTruthy();
  });

  /**
   * unit test ngOnInit should thumbUpIcon be false and negativePercentage 90%
   * @author cramos
   */
  it('ngOnInit should set percentage copies', () => {
    const mockedVotesInfo = {
      id: '123',
      name: 'name',
      votes: {
        likes: 10,
        dislikes: 90
      },
      timeInProfetion: '10 months',
      profession: 'research',
      description: 'ops',
      urlPicture: '/some'
    };
    component.voteInfo = mockedVotesInfo;
    const mockUtilService = TestBed.inject(UtilService);
    spyOn(mockUtilService, 'getPercentage').and.returnValue('10');
    component.ngOnInit();
    expect(component.thumbUpIcon).toBeFalsy();
    expect(component.voteSectionCopies.negativePercentage).toBe('10%');
  });
});
