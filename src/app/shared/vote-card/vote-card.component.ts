import { Component, Input, OnInit } from '@angular/core';
import { VoteSectionCopies } from 'src/app/models/vote-section-copies.model';
import { AllowingVote, VotesInfo } from 'src/app/models/votes-info.model';
import { VotesService } from 'src/app/services/votes-service';
import { VOTESOPTIONS } from 'src/assets/constants/constants';
import { UtilService } from '../utils/util-service';

@Component({
  selector: 'app-vote-card',
  templateUrl: './vote-card.component.html',
  styleUrls: ['./vote-card.component.scss']
})
export class VoteCardComponent implements OnInit {
  @Input() voteInfo: VotesInfo;
  allowVoting: AllowingVote;
  voteSectionCopies: VoteSectionCopies;
  thumbUpIcon: boolean;
  readonly votesOptions: typeof VOTESOPTIONS = VOTESOPTIONS;

  constructor(
    private votesInfoService: VotesService,
    private utilService: UtilService
  ) {
    this.allowVoting = new AllowingVote(true, true);
    this.voteSectionCopies = {
      buttonLabel: 'Vote now',
      positivePercentage: '',
      negativePercentage: ''
    };
    this.voteInfo = {
      id: '',
      name: '',
      votes: {
        likes: 0,
        dislikes: 0
      },
      timeInProfetion: '',
      profession: '',
      description: '',
      urlPicture: ''
    };
    this.thumbUpIcon = false;
  }

  ngOnInit(): void {
    if (this.voteInfo !== undefined) {
      this.setPercentageCopies();
    }
  }

  /**
   * update the vote information
   * @param voteType like or dislike
   * @author cramos
   */
  vote(voteType: string): void {
    switch (voteType) {
      case VOTESOPTIONS.LIKE:
        this.voteInfo.votes.likes++;
        if (!this.allowVoting.dislike) {
          this.voteInfo.votes.dislikes--;
        }
        this.allowVoting = new AllowingVote(false, true);
        break;
      case VOTESOPTIONS.DISLIKE:
        this.voteInfo.votes.dislikes++;
        if (!this.allowVoting.like) {
          this.voteInfo.votes.likes--;
        }
        this.allowVoting = new AllowingVote(true, false);
        break;
      default:
        break;
    }
    this.setPercentageCopies();
  }

  /**
   * submit the information to the service
   * @author cramos
   */
  submitVote(clickEvent: any): void {
    this.votesInfoService.setVotesInfo(this.voteInfo).then((res: any) => {
      if (res) {
        clickEvent.preventDefault();
        clickEvent.stopPropagation();
        this.allowVoting = new AllowingVote(true, true);
        this.voteSectionCopies.buttonLabel = 'Vote again';
        this.voteInfo.description = 'Thank you for voting!';
      } else {
        console.log(`no se pudo votar ${res}`);
      }
    }, err => {
      if (err) {
        console.log(`no se pudo votar ${err}`);
      }
    });
  }

  /**
   * calculate and return style for render the bar
   * @param likeBar like bar or dislike
   * @author cramos
   */
  calculateBar(likeBar: boolean): object {
    const currentPercentage = `${this.utilService.getPercentage(this.voteInfo.votes, likeBar)}%`;
    return {
      width: currentPercentage
    };
  }

  /**
   * calculate percentage and set copies of percentage
   * @param voteType like or dislike
   * @author cramos
   */
  setPercentageCopies(): void {
    const positiveVotes = this.utilService.getPercentage(this.voteInfo.votes, true);
    this.thumbUpIcon = positiveVotes >= '50' ? true : false;
    this.voteSectionCopies.positivePercentage = `${positiveVotes}%`;
    this.voteSectionCopies.negativePercentage = `${this.utilService.getPercentage(this.voteInfo.votes, false)}%`;
  }

}
