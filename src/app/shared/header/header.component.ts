import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit {

  domElMenu: HTMLElement;

  ngAfterViewInit(): void {
    this.domElMenu = document.getElementById('menu');
  }

  toggleMenu(): void {
    this.domElMenu.classList.toggle('active');
  }

}
