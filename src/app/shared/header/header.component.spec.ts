import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * unit test ngAfterViewInit should get Dom element with id menu
   * @author cramos
   */
  it('ngAfterViewInit should obtain dom element by id', () => {
    component.ngAfterViewInit();
    expect(component.domElMenu).toBeDefined();
  });

  /**
   * unit test toggleMenu should add class active
   * @author cramos
   */
  it('ngAfterViewInit should obtain dom element by id', () => {
    component.toggleMenu();
    expect(component.domElMenu.classList.contains('active')).toBeTruthy();
  });
});
