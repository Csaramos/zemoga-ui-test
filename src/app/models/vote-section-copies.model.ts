export type VoteSectionCopies = {
    positivePercentage: string;
    negativePercentage: string;
    buttonLabel: string
  };
