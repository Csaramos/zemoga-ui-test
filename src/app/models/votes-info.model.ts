export type VotesInfo = {
  id: string,
  name: string,
  votes: VotesTypes,
  timeInProfetion: string,
  profession: string,
  description: string,
  urlPicture: string
};

export type VotesTypes = {
  likes: number,
  dislikes: number
};

export class AllowingVote {
  like: boolean;
  dislike: boolean;

  constructor(like: boolean, dislike: boolean) {
    this.like = like;
    this.dislike = dislike;
  }
}
