import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { HowItWorksComponent } from './pages/how-it-works/how-it-works.component';
import { PastTrialsComponent } from './pages/past-trials/past-trials.component';



const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'past-trials',
    component: PastTrialsComponent
  },
  {
    path: 'how-it-works',
    component: HowItWorksComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
