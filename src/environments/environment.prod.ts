import { credentials } from 'src/credentials';

export const environment = {
  production: true,
  firebaseConfig: credentials.firebaseConfig
};
