export const COLLECTIONS = {
  VOTES: 'votesCollection',
};
export const VOTESOPTIONS = {
  LIKE: 'like',
  DISLIKE: 'dislike'
};
export const URLSCOPIES = {
  HOME: '../../assets/content/homeCopies.json'
};
